require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get about_us" do
    get :about_us
    assert_response :success
  end

  test "should get services" do
    get :services
    assert_response :success
  end

  test "should get catalog" do
    get :catalog
    assert_response :success
  end

  test "should get shipping" do
    get :shipping
    assert_response :success
  end

  test "should get sales" do
    get :sales
    assert_response :success
  end

  test "should get for_designers" do
    get :for_designers
    assert_response :success
  end

  test "should get contacts" do
    get :contacts
    assert_response :success
  end

end
