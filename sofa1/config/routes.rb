Sofa1::Application.routes.draw do
  get "pages/index"
  get "pages/about_us"
  get "pages/services"
  get "pages/catalog"
  get "pages/shipping"
  get "pages/sales"
  get "pages/for_designers"
  get "pages/contacts"
  post "pages/send_feedback"#, controller: "pages", action: "send_feedback"
  #get "pages/send_feedback"

  get "main/about_us"


  root 'pages#index'

  get "main/index2"
  get "main/index3"

  #resources :images
  get "images/new"
  get "images/index"
  get "images/get"
  get "images/create"
  get "images/edit"
  get "images/delete"
  get "images/refresh", controller: "images", action: "refresh"

  resources :products
  #get '/[:products]/show/:id'#, controller:'products', action:'show'
  get '/products/new'
  get '/products/create'

  resources :categories
  #get '/categories'
  get '/categories/new'
  get '/categories/create'

  resources :images

  get '/signup', controller: :users, action: :new
  get '/edit_profile', controller: :users, action: :edit
  get '/add_to_cart/:id', to: 'carts#add_product', as: :add_to_cart
  get '/remove_from_cart/:id', to: 'carts#remove_product', as: :remove_from_cart
  get '/remove_line_from_cart/:id', to: 'carts#remove_line', as: :remove_line_from_cart
  get '/refresh_cart', to: 'carts#refresh', as: :refresh_cart, defaults: {format: :js}
  resources :users, except: [:new, :edit, :add_to_cart]
  #get '/signup', controller: :users, action: :new

  get '/my_cart', controller: :carts, action: :show, as: :my_cart

  get '/login', controller: :users, action: :login
  #post '/auth', controller: :users, action: :auth

  get '/nimda', controller: :admin, action: :authenticate
  get '/logout', controller: :admin, action: :logout

  get '/signin', controller: :sessions, action: :new
  post '/signin', controller: :sessions, action: :create
  get '/signout', controller: :sessions, action: :destroy

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: products.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
