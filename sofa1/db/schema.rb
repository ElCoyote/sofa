# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140223115120) do

  create_table "carts", force: true do |t|
    t.integer "user_id"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories_products", id: false, force: true do |t|
    t.integer "product_id",  null: false
    t.integer "category_id", null: false
  end

  create_table "fabric_categories", force: true do |t|
    t.integer "price"
  end

  create_table "fabrics", force: true do |t|
    t.string  "name"
    t.string  "description"
    t.integer "fabric_categories_id"
  end

  create_table "images", force: true do |t|
    t.string   "file_path"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "logs", force: true do |t|
    t.string   "path_info"
    t.string   "query_string"
    t.string   "remote_addr"
    t.string   "remote_host"
    t.string   "request_method"
    t.string   "request_uri"
    t.string   "server_name"
    t.string   "server_port"
    t.string   "server_protocol"
    t.string   "server_software"
    t.string   "http_host"
    t.string   "http_accept"
    t.string   "http_accept_language"
    t.string   "http_accept_encoding"
    t.string   "http_referer"
    t.string   "http_cookie"
    t.string   "http_connection"
    t.string   "http_if_not_match"
    t.string   "request_path"
    t.string   "original_full_path"
    t.string   "dispatch_request_parameters"
    t.string   "calculated_location"
    t.text     "all_information"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "main_slides", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "link"
    t.string   "image_url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "positions", force: true do |t|
    t.integer  "cart_id"
    t.integer  "product_id"
    t.integer  "fabric_id"
    t.integer  "quantity",   default: 1, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "positions", ["cart_id", "product_id"], name: "index_positions_on_cart_id_and_product_id", unique: true

  create_table "product_images", force: true do |t|
    t.integer "product_id"
    t.integer "image_id"
    t.integer "priority",   default: 5
  end

  create_table "products", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "mechanism_id"
    t.string   "image_file"
    t.text     "sizes"
    t.decimal  "position"
    t.integer  "price",        default: 0
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.integer  "role_id"
    t.string   "name"
    t.string   "surname"
    t.string   "patronymic"
    t.string   "email"
    t.string   "password_digest"
    t.string   "phone"
    t.string   "phone2"
    t.string   "address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
