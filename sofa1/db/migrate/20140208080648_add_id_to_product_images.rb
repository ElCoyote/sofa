class AddIdToProductImages < ActiveRecord::Migration
  def change
    add_column :product_images, :id, :primary_key
  end
end
