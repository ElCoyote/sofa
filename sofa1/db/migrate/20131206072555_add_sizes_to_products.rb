class AddSizesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :sizes, :text
  end
end
