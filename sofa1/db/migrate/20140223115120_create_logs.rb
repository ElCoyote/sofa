class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :path_info
      t.string :query_string
      t.string :remote_addr
      t.string :remote_host
      t.string :request_method
      t.string :request_uri
      t.string :server_name
      t.string :server_port
      t.string :server_protocol
      t.string :server_software
      t.string :http_host
      t.string :http_accept
      t.string :http_accept_language
      t.string :http_accept_encoding
      t.string :http_referer
      t.string :http_cookie
      t.string :http_connection
      t.string :http_if_not_match
      t.string :request_path
      t.string :original_full_path
      t.string :dispatch_request_parameters
      t.string :calculated_location
      t.text   :all_information
      t.timestamps
    end
  end
end
