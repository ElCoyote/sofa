class CreateMainSlides < ActiveRecord::Migration
  def change
    create_table :main_slides do |t|
      t.string :name
      t.text :description
      t.string :link
      t.string :image_url

      t.timestamps
    end
  end
end
