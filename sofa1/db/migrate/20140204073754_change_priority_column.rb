class ChangePriorityColumn < ActiveRecord::Migration
  def change
    change_column :product_images, :priority, :integer, default: 1
  end
end
