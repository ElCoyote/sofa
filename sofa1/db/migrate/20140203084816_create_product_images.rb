class CreateProductImages < ActiveRecord::Migration
  def change
    create_table :product_images, id: false do |t|
      t.references :product
      t.references :image
      t.integer :priority
    end
    add_index :product_images, [:product_id, :image_id]
    add_index :product_images, [:image_id, :product_id]
  end
end
