class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.references :cart
      t.references :product
      t.references :fabric
      t.integer    :quantity, null: false, default: 1
      t.timestamps
    end
    add_index :positions, [:cart_id, :product_id], unique: true
  end
end
