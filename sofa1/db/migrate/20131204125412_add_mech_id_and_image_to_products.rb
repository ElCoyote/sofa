class AddMechIdAndImageToProducts < ActiveRecord::Migration
  def change
    add_column :products, :mechanism_id, :decimal
    add_column :products, :image_file, :string
  end
end
