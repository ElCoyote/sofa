class ChangePriorityDefault < ActiveRecord::Migration
  def change
    change_column :product_images, :priority, :integer, default: 5
  end
end
