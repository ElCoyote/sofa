class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.references :role
      t.string :name
      t.string :surname
      t.string :patronymic
      t.string :email, unique: true
      t.string :password_digest
      t.string :phone
      t.string :phone2
      t.string :address

      t.timestamps
    end
  end
end
