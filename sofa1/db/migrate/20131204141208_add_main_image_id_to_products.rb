class AddMainImageIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :main_image_id, :decimal
  end
end
