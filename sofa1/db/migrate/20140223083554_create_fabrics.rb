class CreateFabrics < ActiveRecord::Migration
  def change
    create_table :fabrics do |t|
      t.string :name
      t.string :description
      t.references :fabric_categories
    end
  end
end
