class SessionsController < ApplicationController
  protect_from_forgery
  def new
    @user = User.new
  end
  def create
    #@user = User.find_by_email(signin_params[:email].downcase)
    if (@user = User.find_by_email(signin_params[:email].downcase)) && @user.authenticate(signin_params[:password])
      session[:user] = @user.id
      session[:name] = @user.name
      if @user.cart.positions.empty?
        if session[:cart]
          session[:cart].each_pair {|key, value|
            @user.cart.positions << (Position.new product_id: key, quantity: value)
          }

        end
      end
      redirect_to :root
      #if request.headers['HTTP_REFERER'] then render request.headers['HTTP_REFERER'] else redirect_to :root end
    else
      #@user? @user = nil
      if @user then @user = nil end
      session[:user] = nil
      session[:name] = nil
      flash[:notice] = "Неверный email или пароль."
      render file: "users/user_error"
    end
  end
  def index

  end
  def destroy
    if @user then @user = nil end
    session[:user] = nil
    session[:name] = nil
    redirect_to :root
    #if request.headers['HTTP_REFERER'] then render request.headers['HTTP_REFERER'] else redirect_to :root end
  end

  protected
  def signin_params
    params.require(:user).permit(:email, :password)
  end
end
