class PagesController < ApplicationController

  skip_before_action :verify_authenticity_token

  def index
    @scripts = ['bigslider', 'tabs']
  end

  def about_us
    @scripts = ['tabs']
  end

  def services
  end

  def catalog
  end

  def shipping
  end

  def sales
  end

  def for_designers
  end

  def contacts
    puts params
    #redirect_to pages_contacts_path
    #render pages_contacts_path
    @feedbacks = []
  end
  def send_feedback
    puts "feedbacked"
    #render :contacts
  end

end
