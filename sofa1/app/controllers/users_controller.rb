class UsersController < ApplicationController
  protect_from_forgery
  #before_action :user_exists?, only: [:create]
  before_action :is_admin?, only: [:index]
  before_action :user_is_signed_up?, only: [:new]
  #before_action :session_user_id_against_params_id?, only: [:edit, :delete, :destroy]

  def index
    #@users = User.all.includes(:roles)
    @users = User.includes(:role).all
  end
  def show

  end
  def new
    @user = User.new
  end
  def create
    @user = User.new user_params
    puts @user.errors.inspect
    if @user.save
      flash[:notice] = "Пользователь удачно создан"
      if session[:admin] then
        redirect_to :users
      else
        #session[:user] = @user.id
        render file: 'users/user_error'
      end
    else
      flash[:notice] = "Пользователь не создан"
      render file: 'users/user_error'
    end
    flash["notice"] = "Test notice"
    #redirect_to :users
  end
  def edit
    if session[:user]
      @user = User.find session[:user]
    else
      @user = nil
      flash[:notice] = "Пользователь не обнаружен"
      render file: "users/user_error"
    end
  end
  def update
    @user = User.find session[:user]
    @user.current_password = params[:user][:current_password]
    if @user.update user_params
      flash[:notice] = "Данные успешно изменены."
      render file: "users/user_error"
    else
      #flash[:notice] = "Данные успешно изменены."
      render file: "users/user_error"
    end
  end
  def delete
  end
  def destroy
  end
  def login
    @user = User.new
  end

  protected
  def user_params
    params.require(:user).permit(:name, :surname, :patronymic,
                                 :email, :phone, :phone2, :address,
                                 :password, :password_confirmation, :role_id)
  end



  def user_is_signed_up?
    if is_user? then
      flash[:notice] = "Вы уже зарегистрированы в системе."
      render file: 'users/user_error'
    end
  end

  # временно не используется. а может и не временно
  def session_user_id_against_params_id?
    if session[:user] != params[:id].to_i && !session[:admin]
      render file: 'public/ru/404.html', status: 404
    end
  end

# не используется
  def user_exists?
    if user_params[:name].blank?
      flash[:notice] = "Не указано имя пользователя."
      render file: 'users/user_error'
    elsif user_params[:surname].blank?
      flash[:notice] = "Не указана фамилия пользователя."
      render file: 'users/user_error'
    elsif user_params[:email].blank?
      flash[:notice] = "Не указан адрес электронной почты пользователя."
      render file: 'users/user_error'
    elsif user_params[:password].blank?
      flash[:notice] = "Не указан пароль пользователя."
      render file: 'users/user_error'
    elsif user_params[:password_confirmation].blank?
      flash[:notice] = "Не указано подтверждение пароля пользователя."
      render file: 'users/user_error'
    elsif user_params[:password] != user_params[:password_confirmation]
      flash[:notice] = "Пароль и подтверждение пароля не совпадают."
      render file: 'users/user_error'
    end
    if User.find_by_email params[:user][:email] then
      flash[:notice] = "Пользователь с таким e-mail уже зарегистрирован в системе."
      render file: 'users/user_error'
    end
  end

end
