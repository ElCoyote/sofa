  class ProductsController < ApplicationController

  # /products GET
  before_filter :del_empty_categories, only:[:create, :update]
  skip_before_filter :verify_authenticity_token, only: [:destroy]
  #http_basic_authenticate_with name: login, password: password, except: [:index, :show]
  before_action :is_admin?, only: [:new, :edit, :create]

  def index
    @products = Product.all
    @positions = ProductImage.all
  end

  # /product/1 GET

  def show
    @params = params
    @product = Product.find(params[:id])
    @price = @product.price

    respond_to do |format|
      format.html
      #format.js {render template: 'layouts/show'}

    end
  end

  # /product/new GET
  def new
    @categories = Category.all
    @product = Product.new
    @products = Product.all
    @image = Image.new
    @images = Image.all
  end

  # /products/1/edit GET
  def edit
    @product = Product.find(params[:id])
    @categories = Category.all
    @images = Image.all
    @maximal_priority = @product.product_images.order(:priority).first
    @positions = ProductImage.all

  end

  # /products POST
  def create
    #TODO: Добавление фото
    @product = Product.new(product_params)
    @categories = Category.find( params[:product][:category_ids] )
    @product.categories = @categories
    @images = Image.find(params[:image_checked].keys)
    checked = params[:image_checked].keys
    checked.each { |i|
      im = Image.where(id: i).first
      @position = ProductImage.new product: @product, image: im, priority: params[:image_priority][i]
      @position.save
    }

    redirect_to :products
  end

  # /products/1 PUT
  def update
    @product = Product.includes(:product_images).includes(:images).find (params[:id])
    @categories = Category.find( params[:product][:category_ids] )
    @product.categories = @categories
    #@product.main_image_id = params['main_image']
    @images = Image.find(params[:image_checked].keys.to_a)
    @product.images = @images
    checked = params[:image_checked].keys
    checked.each {|i|
      pr_img = @product.product_images.find_by image_id: i
      ProductImage.update pr_img, priority: params[:image_priority][i]
    }
    @product.update (product_params)
    @product.save
    redirect_to :edit_product
  end

  # /products/1 DELETE
  def destroy
    @product = Product.find(params[:id])
    @product.delete
    redirect_to :products
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, :sizes, :category_ids, :price)
  end

  def del_empty_categories
    params[:product][:category_ids].delete("")
  end

end
