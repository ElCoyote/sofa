class AdminController < ApplicationController
  #http_basic_authenticate_with name: login, password: password, except: [:logout]
  #before_action :authenticate
  def authenticate
    authenticate_or_request_with_http_basic "Введите логин и пароль" do |email, pass|
      puts email
      puts pass
      if email && pass
        if (user = User.find_by_email(email.downcase))
          if user.role_id == 1 && user.authenticate(pass)
            session[:admin] = true
            redirect_to :root
          else
            flash[:notice] = "Could not found login or password. RP"
            session[:admin] = false
            request_http_basic_authentication flash[:notice]
          end
        else
          flash[:notice] = "Could not found login or password. L"
          session[:admin] = false
          request_http_basic_authentication flash[:notice]
        end
      end
    end
  end

  def logout
    session[:admin] = false
    if request.headers['HTTP_REFERER']
      redirect_to request.headers['HTTP_REFERER']
    else
      redirect_to :root
    end
  end
end
