class CategoriesController < ApplicationController
  # /categories GET
  def index
    @categories = Category.all
  end
  def new
    @category = Category.new
  end
  def create
    @categories = Category.new(category_params)
    @categories.save;
  end
  def category_params
    params.require(:category).permit(:name, :description)
  end
end
