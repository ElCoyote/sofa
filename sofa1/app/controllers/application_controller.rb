class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #layout 'application.html.slim'
  before_action :add_current_routes
  before_action :save_log

  protected

  def self.login
    "admin"
  end
  def self.password
    "admin"
  end

  def add_current_routes
    #session[:routes].push {controller: params[:controller], action: params[:action]}
    puts session[:user]
    puts session[:admin]
  end
  def current_user
    if session[:user]
      if user = User.find(session[:user])
        user
      else
        nil
      end
    else
      nil
    end
  end

  def get_cart_items
    items = []
    if user = current_user
      user.cart.positions.each {|i|
        items << {id: i.product_id,
                  name: i.product.name,
                  quantity: i.quantity,
                  price: i.product.price}
      }
      return items
    elsif session[:cart]
      #positions = Position.includes(:product).where product_id: session[:cart].keys
      products = Product.where id: session[:cart].keys
      session[:cart].each_pair {|key, value|
        product = products.find_by(id: key)
        items << {id: key,
                  name: product.name,
                  quantity: session[:cart][key],
                  price: product.price}
      }
      return items
    end
  end

  def is_admin?
    render file: 'public/ru/404.html', status: 404 unless session[:admin] == true
  end
  def is_user?
    if session[:user] then true end
  end

  def save_log
    Log.create(path_info:            request.headers['PATH_INFO'],
               query_string:         request.headers['QUERY_STRING'],
               remote_addr:          request.headers['REMOTE_ADDR'],
               remote_host:          request.headers['REMOTE_HOST'],
               request_method:       request.headers['REMOTE_METHOD'],
               request_uri:          request.headers['REQUEST_URI'],
               server_name:          request.headers['SERVER_NAME'],
               server_port:          request.headers['SERVER_PORT'],
               server_protocol:      request.headers['SERVER_PROTOCOL'],
               server_software:      request.headers['SERVER_SOFTWARE'],
               http_host:            request.headers['HTTP_HOST'],
               http_accept:          request.headers['HTTP_ACCEPT'],
               http_accept_language: request.headers['HTTP_ACCEPT_LANGUAGE'],
               http_accept_encoding: request.headers['HTTP_ACCEPT_ENCODING'],
               http_referer:         request.headers['HTTP_REFERER'],
               http_cookie:          request.headers['HTTP_COOKIE'],
               http_connection:      request.headers['HTTP_CONNECTION'],
               http_if_not_match:    request.headers['HTTP_IF_NOT_MATCH'],
               request_path:         request.headers['REQUEST_PATH'],
               original_full_path:   request.headers['ORIGINAL_FULL_PATH'],
               dispatch_request_parameters:   request.headers['action_dispatch.request.parameters'].to_s,
               calculated_location: "Unknown",  #TODO: реализовать вычисление местоположения
               all_information: request.headers.env.to_a.join(";\n "))
  end
end
