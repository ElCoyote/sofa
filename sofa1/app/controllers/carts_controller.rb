class CartsController < ApplicationController
  def show
    #if session[:user]
    #  user = User.includes(:cart).find session[:user]
    #  @positions = user.cart.positions
    #elsif session[:cart]
    #
    #end
    @items = get_cart_items
    @cart_sum  = 0
    if @items.any?
      @items.each {|i|
      @cart_sum += i[:quantity] * i[:price]
      }
    end
    #respond_to {|format|
    #  #format.js { render partial: 'carts/show.js.erb', locals: {js: true}}
    #  format.html { render 'show', locals: {js: true}}
    #}
    ##render template: 'carts/_show.js.erb', locals: {js: true}
  end
  def add_product
    if session[:user]
      if user = User.find(session[:user])
        user.add_to_cart params[:id]
        #respond_to {|format|
        #  format.html { render controller: :products, action: :show, id: params[:id]}
        #}
        #render 'carts/total_price', locals: {total_sum: user.cart.sum}
      else
        flash[:notice] = 'Нет такого пользователя'
        render file: 'users/user_error'
      end
    else
      #session[:cart] = {product_id: params[:id], if session[:cart][:quantity] then session[:cart][:quantity]++ else quantity: 1}
      if session[:cart].blank?
        session[:cart] = {params[:id].to_i => 1}
      else
        if session[:cart][params[:id].to_i]
          session[:cart][params[:id].to_i] = session[:cart][params[:id].to_i] + 1
        else
          session[:cart][params[:id].to_i] = 1
        end
      end
    end
    @items = get_cart_items
    render :refresh
  end


  def remove_product
    if user = current_user
      user.remove_from_cart params[:id]
    elsif session[:cart]
      if session[:cart][params[:id].to_i] && session[:cart][params[:id].to_i] > 0
        session[:cart][params[:id].to_i] = session[:cart][params[:id].to_i] - 1
      elsif session[:cart][params[:id].to_i] &&  session[:cart][params[:id].to_i] == 0
        session[:cart].delete params[:id].to_i
      end
    end
    #respond_to {|format|
    #  format.js { render template: 'carts/refresh'}
    #  format.html {render :show}
    #}
    @items = get_cart_items
    render :refresh
  end

  def remove_line
    if session[:user] && (user = User.includes(cart: :positions).where(id: session[:user]).first)
      user.cart.positions.where(product_id: params[:id]).first.delete
    elsif session[:cart]
      if session[:cart][params[:id].to_i]
        session[:cart].delete params[:id].to_i
      end
    end
    @items = get_cart_items
    render :refresh
  end

  def refresh
    render partial: "carts/total_price", locals: {total_sum: 500}
  end

  #def get_cart_items
  #  items = []
  #  if user = current_user
  #    user.cart.positions.each {|i|
  #      items << {id: i.product_id,
  #                name: i.product.name,
  #                quantity: i.quantity,
  #                price: i.product.price}
  #    }
  #    return items
  #  elsif session[:cart]
  #    #positions = Position.includes(:product).where product_id: session[:cart].keys
  #    products = Product.where id: session[:cart].keys
  #    session[:cart].each_pair {|key, value|
  #      product = products.find_by(id: key)
  #      items << {id: key,
  #                name: product.name,
  #                quantity: session[:cart][key],
  #                price: product.price}
  #    }
  #    return items
  #  end
  #end
end