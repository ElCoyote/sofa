module ApplicationHelper
  def logo
    link_to(image_tag("logo.png", :class => 'logo'), root_path)
  end
  def basket

  end
  def sofas_imgs_path
    "sofas/imgs/"
  end
  def roubles sum, options: :nominative
    splitted = sum.to_s.split ""
    if options == :nominative then
      if (10..20).include?(splitted.last(2).join.to_i) or (5..9).include?(splitted.last.to_i) or splitted.last.to_i == 0
        "рублей"
      elsif splitted.last.to_i == 1
        "рубль"
      elsif (2..4).include? splitted.last.to_i
        "рубля"
      end
    elsif options == :genitive
      if splitted.last.to_i == 1
        "рубля"
      else
        "рублей"
      end
    end
  end

  def admin?
    session[:admin]
  end

  def admin=(admin_mode)
    #AdminController.is_admin = admin_mode
    session[:admin] = admin_mode
  end

  def user?
    if session[:user] then true else false end
  end
  def current_user
    if session[:user] && (user = User.find session[:user])
      user
    else
      nil
    end
  end
  def current_user_id=(user_id)
    seession[:user] = user_id
  end

  def total_price
    if user = current_user
      if user.cart.sum
        user.cart.sum
      end
    elsif session[:cart] && products = Product.where(id: session[:cart].keys)
      sum = 0
      products.each {|i|
        sum += (session[:cart][i.id] * i.price).to_i
      }
      sum
    else
      0
    end
  end

end
