module PagesHelper
  def article_styles
    stylesheet_link_tag 'css/article'
  end
  def about_us_styles
    stylesheet_link_tag 'css/about_us'
  end
end
