class User < ActiveRecord::Base

  attr_accessor :current_password

  belongs_to :role
  has_one :cart

  has_secure_password

  validates :name, presence: true, length: {maximum: 100}
  validates :surname, presence: true, length: {maximum: 100}
  validates :email, presence: true, uniqueness: {case_sensitive: false}, length: {maximum: 100}, format: {with: /@/}, on: :create
  validates :email, presence: true, length: {maximum: 100}, format: {with: /@/}, on: :edit
  validates :password, length: {minimum: 6}
  validates :password_confirmation, length: {minimum: 6}
  validate  :current_password_is_valid?, on: :update
  validates :current_password, presence: true, on: :update

  after_create :make_cart

  def is_admin?
    if user.role_id == 1 then
      true
    else
      false
    end
  end


  def add_to_cart product_id
    product = Product.find product_id
    cart.add_product product
  end

  def remove_from_cart product_id
    product = Product.find product_id
    cart.remove_product product
  end

  protected
  def current_password_is_valid?
    if self.authenticate(current_password)
      true
    else
      errors.add(:current_password, :invalid)
      false
    end
  end

  def make_cart
    Cart.create user:self
  end

end
