class Product < ActiveRecord::Base
  validates :name, :presence => true
  validates :description, :presence => true


  has_and_belongs_to_many :categories

  has_many :product_images, class_name: "ProductImage"
  has_many :images, through: :product_images

  has_many :positions
  has_many :carts, through: :positions

  def main_image
    return product_images.order(:priority).first.image
  end

  def add_image image, position
    if product_images.include? image
      im = product_images.where(image_id: image.id).first
      im.priority = position
    else
      product_images.create image: image, priority: position
    end
  end

  # кажется, не используется
  def roubles (sum = price, options: :nominative)
    splitted = sum.to_s.split ""
    if options == :nominative then
      if (10..20).include?(splitted.last(2).join.to_i) or (5..9).include?(splitted.last.to_i) or splitted.last.to_i == 0
        "рублей"
      elsif splitted.last.to_i == 1
        "рубль"
      elsif (2..4).include? splitted.last.to_i
        "рубля"
      end
    elsif options == :genitive
      if splitted.last.to_i == 1
        "рубля"
      else
        "рублей"
      end
    end

  end

end