class Cart < ActiveRecord::Base
  belongs_to :user
  has_many :positions
  has_many :products, through: :positions

  def add_product product
    if products.include? product
      positions.where(product_id: product.id).first.increment! :quantity
    else
      positions.create product: product, quantity: 1
    end
  end

  def remove_product product
    if position = positions.find_by(product_id: product.id)
      if position.quantity == 0
        position.delete
      else
        position.decrement :quantity
        position.save
      end
      true
    else
      false
    end
  end

  def sum
    @sum = 0
    positions.map {|i| @sum += i.product.price * i.quantity}
    return @sum
  end
end