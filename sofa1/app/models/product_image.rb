#class ImagesProducts < ActiveRecord::Base
#  belongs_to :product, polymorphic: true
#  belongs_to :image, polymorphic: true
#end

class ProductImage < ActiveRecord::Base
  self.table_name = "product_images"
  belongs_to :image
  belongs_to :product
end