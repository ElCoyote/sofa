class Image < ActiveRecord::Base

  has_many :products, through: :product_images
  has_many :product_images, class_name: "ProductImage"

  #accepts_nested_attributes_for :product_images, :allow_destroy => true
  #attr_accessible :position

  #def position
  #  product_images.position
  #  p "Test Position"
  #end

  def file_path
    "sofas/imgs/" + super
  end

  def get_priority
    if pi = product_images.where(image_id: self.id).first
      return pi.priority
    end
  end

  def set_priority pr
    #pi = product_images.where(image_id: self.id).first
    #puts pi
    if pi = product_images.where(image_id: self.id).first
      pi.priority = pr
      #product_images.where(image_id: self.id).first.priority = pr
      #product_images.update_attributes priority: pr
      #ProductImage.where(product_id: pi.product_id).update
      #pi.update_attributes priority: pr
      pi.save
      puts pi
    end
  end

end

