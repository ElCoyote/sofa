/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var slidertimer;
//            var offset; 
var current_pos = 0;
init_slider = function (){
    sliders = document.getElementsByClassName("slider");
    for (i = 0; i < sliders.length; i++) {
        sliders[i].slides = sliders[i].getElementsByClassName("slide");
        sliders[i].fullWidth = 0;
        if (sliders[i].clientWidth === 0) 
                sliders[i].style.width = sliders[i].slides[0].clientWidth.toString() + "px";
        if (sliders[i].clientHeight === 0) 
                sliders[i].style.height = sliders[i].slides[0].clientHeight.toString() + "px";
        for (j = 0; j < sliders[i].slides.length; j++) {
            sliders[i].fullWidth = sliders[i].fullWidth + sliders[i].slides[j].clientWidth;
        }

        sliders[i].slideContainer = sliders[i].getElementsByClassName("slide_container");
        sliders[i].slideContainer[0].style.width = sliders[i].fullWidth.toString() + "px";    
//                    sliders[i].slideContainer[0].prototype.pos = 0;
        sliders[i].slideContainer[0].step = sliders[i].slides[0].clientWidth;
        sliders[i].slideContainer[0].left_pos = 0;

        sliders[i].arrowLeft = sliders[i].getElementsByClassName("arrow_left");
        sliders[i].arrowRight = sliders[i].getElementsByClassName("arrow_right");
        sliders[i].arrowLeft[0].style.width = (sliders[i].slides[0].clientWidth / 10).toString() + "px";
        sliders[i].arrowLeft[0].style.height = (sliders[i].slides[0].clientHeight).toString() + "px";
        sliders[i].arrowRight[0].style.width = sliders[i].arrowLeft[0].style.width;
        sliders[i].arrowRight[0].style.height = sliders[i].arrowLeft[0].style.height;
        sliders[i].arrowLeft[0].style.marginTop = "-" + sliders[i].arrowLeft[0].style.height;
        sliders[i].arrowRight[0].style.marginTop = sliders[i].arrowLeft[0].style.marginTop;
        sliders[i].last_slide_num = sliders[0].slideContainer[0].children.length - 1;
    }
};
on_arrow_click = function(this_element) {
    currentSlider = this_element.parentElement.parentElement;
//                if (offset === "undefined") offset = 0;

    if (this_element.className === "arrow_left") { // если нажата левая стрелка
        current_pos--;
//                    offset = 0;
//                    if (current_pos > currentSlider.slideContainer.children().length)  {
        if (current_pos < 0)  {
            current_pos = 0 ;
            currentSlider.slideContainer[0].
                    insertBefore(currentSlider.slideContainer[0].children[currentSlider.last_slide_num], currentSlider.slideContainer[0].children[0]);

            current_pos = 0;
            currentSlider.slideContainer[0].style.left = "-"+currentSlider.slideContainer[0].step.toString()+"px";
            currentSlider.slideContainer[0].left_pos = -currentSlider.slideContainer[0].step;
        } else {

        }
        clearInterval(slidertimer);
        slidertimer = setInterval(move_left, 5);
    }
    else { // если нажата правая стрелка
        current_pos++;
        if (current_pos > currentSlider.last_slide_num)  {
            current_pos = currentSlider.last_slide_num;
            currentSlider.slideContainer[0].
                    appendChild(currentSlider.slideContainer[0].children[0]);
            current_pos = currentSlider.last_slide_num;
            currentSlider.slideContainer[0].left_pos = currentSlider.slideContainer[0].left_pos + currentSlider.slideContainer[0].step;
            currentSlider.slideContainer[0].style.left = currentSlider.slideContainer[0].left_pos.toString()+"px";
        } 

        clearInterval(slidertimer);
        slidertimer = setInterval(move_right, 5);
    }
};
move_left = function () {
//                offset += 5;
    currentSlider.slideContainer[0].left_pos += 30;
    currentSlider.slideContainer[0].style.left = currentSlider.slideContainer[0].left_pos.toString()+"px";
    if ((currentSlider.slideContainer[0].left_pos > 0)) {
        currentSlider.slideContainer[0].left_pos = 0;
        currentSlider.slideContainer[0].style.left = currentSlider.slideContainer[0].left_pos.toString()+"px";
        clearInterval(slidertimer);
    } else if (currentSlider.slideContainer[0].left_pos > - current_pos * sliders[0].clientWidth){
        currentSlider.slideContainer[0].left_pos = -sliders[0].clientWidth * current_pos;
        currentSlider.slideContainer[0].style.left = currentSlider.slideContainer[0].left_pos.toString()+"px";
        clearInterval(slidertimer);
    }
};

move_right = function () {
//                offset -= 5;
    currentSlider.slideContainer[0].left_pos -= 30;
    currentSlider.slideContainer[0].style.left = currentSlider.slideContainer[0].left_pos.toString()+"px";
    if (currentSlider.slideContainer[0].left_pos < -currentSlider.slideContainer[0].clientWidth) {
        currentSlider.slideContainer[0].left_pos = 0;
        currentSlider.slideContainer[0].style.left = currentSlider.slideContainer[0].left_pos.toString()+"px";
        clearInterval(slidertimer);
    } else if (currentSlider.slideContainer[0].left_pos < -sliders[0].clientWidth * current_pos){
        currentSlider.slideContainer[0].left_pos = - sliders[0].clientWidth*current_pos;
        currentSlider.slideContainer[0].style.left = currentSlider.slideContainer[0].left_pos.toString()+"px";
        clearInterval(slidertimer);
    }
}