/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var $tab_content_box;
var $current_content;

init_tabs = function () {                
    tab_content_box = document.getElementsByClassName("tab_content_box");
    current_content = tab_content_box[0].firstElementChild;

    for (i=1; i< tab_content_box[0].children.length; i++) {
        tab_content_box[0].children[i].style.display = "none";
//                  tab_content_box[0].children[i].style.zIndex = 0;
    }
};

on_click_handler = function (this_element, tab) {
    if (typeof(current_tab) !== "undefined") {
        current_tab.style.backgroundColor = "#ffffff";
        current_tab.style.color = "#7D766A";
        current_content.style.display = "none";
//                current_content.style.zIndex = 0;
    };
    this_element.style.backgroundColor = "#7D766A";
    this_element.style.color = "#ffffff";
    current_content.style.display = "none";
//            current_content.style.zIndex = 0;
    current_content = tab_content_box[0].children[tab];
    current_content.style.display = "block";
//            current_content.style.zIndex = 1;
    current_tab = this_element;
};