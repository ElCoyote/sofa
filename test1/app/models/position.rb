class Position < ActiveRecord::Base
  self.table_name = 'products_images'

  #validates :product_id, presence: true
  #validates :image_id, presence: true

  belongs_to :product
  belongs_to :image

  #accepts_nested_attributes_for :products
  #attr_accessible :product, :product_id, :image, :image_id, :position
end